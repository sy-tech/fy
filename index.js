//自定义数组find，解决浏览器兼容
if (!Array.prototype.find) {
    Array.prototype.find = function (callback) {
        return callback && (this.filter(callback) || [])[0];
    };
}
var jiningdata;
const dataurl1 = 'https://napi.uc.cn/3/classes/feiyan_config/objects?_app_id=59082a00cb2a4ca686cf68f34090c4d1&_fetch=1&_size=500&_max_age=0&_page=1';
const dataurl2 = 'https://napi.uc.cn/3/classes/feiyan_config/objects?_app_id=59082a00cb2a4ca686cf68f34090c4d1&_fetch=1&_size=500&_max_age=0&_page=2';
const dataurl = 'http://wangxianl.cn/sy_config/app/redirect/getContent2?url=https://ncov.dxy.cn/ncovh5/view/pneumonia';
var myChart = echarts.init(document.getElementById('map'));
var chinafeiyan;
var chinafeiyan_max;
var chinafeiyan_min = 0;
var pretitles = {
    'china': '全国',
    'shandong': '山东',
    'jining': '济宁'
}
var prename = decodeURI(getURLPars('p', 'china'));
$('.prename').text(pretitles[prename]);

$.ajaxSettings.async = false;
var bfirst = true;
//丁香园数据
var statistics_data,provs_data,shi_data;
$.get(dataurl,function (res) {
    statistics_data = JSON.parse(/window\.getStatisticsService \=(.*?)\}catch/.exec(res)[1])
    provs_data = JSON.parse(/window\.getListByCountryTypeService1 \=(.*?)\}catch/.exec(res)[1])
    shi_data = JSON.parse(/window\.getAreaStat \=(.*?)\}catch/.exec(res)[1])
});
updateView();
bfirst = false;

$('.change').click(function () {
    prename = $(this).attr('pre')
    $('.prename').text($(this).text());
    $('.provincepanel').empty()
    updateView();
})
//网易
// $.get('http://news.163.com/special/epidemic/',function(res){
// console.log(res)
// })
// //夸克搜索
// $.get('https://api.m.sm.cn/rest?format=json&method=Huoshenshan.healingCity&mapType=1&callback=__jp2',function(res){
// console.log(res)
// })

function updateView() {
    var feiyan_total;
    var res163;
    //注册地图
    $.getJSON(prename + '_map.json', function (mapjson) {
        echarts.registerMap(prename, mapjson);
    });

    //本地数据
    $.getJSON(prename + '_feiyan_total.json', function (data) {
        feiyan_total = data;
    });
    if (prename == 'china') {
        feiyan_total.t_num_quezhen = statistics_data.confirmedCount;
        feiyan_total.t_num_zhiyu = statistics_data.curedCount;
        feiyan_total.t_num_siwang = statistics_data.deadCount;
        feiyan_total.t_num_yisi = statistics_data.suspectedCount;
        feiyan_total.t_title_time = new Date(statistics_data.modifyTime).toLocaleString();
        calcChinaData2(provs_data);
    }
    else if (prename == 'shandong') {
        calcProvTotal(pretitles[prename], provs_data, feiyan_total);
        calcProvData2(pretitles[prename], shi_data);
    }
    else if (prename == 'jining') {
        calcProvTotal('山东', provs_data, feiyan_total);
        $.getJSON(prename + '_feiyan_data.json', function (jsondata) {
            chinafeiyan = jsondata;
        });
        shi_data.forEach(function (data) {
            if (data.provinceShortName == '山东') {
                data.cities.forEach(function (city) {
                    if (city.cityName == pretitles[prename]) {
                        feiyan_total.t_num_quezhen = city.confirmedCount;
                        feiyan_total.t_num_zhiyu = city.curedCount;
                        feiyan_total.t_num_siwang = city.deadCount;
                        feiyan_total.t_num_yisi = city.suspectedCount;
                    }
                })
                // feiyan_total.t_title_time = new Date(data.modifyTime).toLocaleString();
            }
        })
    }
    //UC数据
    // $.get(dataurl1, function (res) {
    //     $.get(dataurl2, function (res2) {
    //         res.data = res.data.concat(res2.data)
    // if (prename == 'china') {
    //     //生成省列表
    //     var provs = provList(res);
    //     calcChinaData(provs, res, feiyan_total);
    //     // $.get('http://124.133.27.90:6082/main/getTotal', function (res) {
    //     //     res163 = res;
    //     //     feiyan_total.t_num_quezhen = /中国（含港澳台）：确诊\s*(\d*)\s*例/.exec($(res).find('.cover_tit_des').text())[1];
    //     //     feiyan_total.t_num_zhiyu = /死亡\s*(\d*)\s*例/.exec($(res).find('.cover_tit_des').text())[1];
    //     //     feiyan_total.t_num_siwang = /治愈\s*(\d*)\s*例/.exec($(res).find('.cover_tit_des').text())[1];
    //     //     feiyan_total.t_title_time = /截止\s*(.*)</.exec($(res).find('.cover_con .tit').html())[1];
    //     // });
    // }
    // else if (prename == 'shandong') {
    //     calcProvData('山东省', res, feiyan_total);
    // }
    // else if (prename == 'jining') {
    //     $.getJSON(prename + '_feiyan_data.json', function (jsondata) {
    //         chinafeiyan = jsondata;
    //     });
    //     res.data.forEach(function (data) {
    //         if (data.two_level_area == '济宁') {
    //             feiyan_total.t_num_quezhen += (data.sure_cnt * 1 || 0);
    //             feiyan_total.t_num_zhiyu += data.cure_cnt * 1 || 0;
    //             feiyan_total.t_num_siwang += data.die_cnt * 1 || 0;
    //             feiyan_total.t_num_yisi += data.like_cnt * 1 || 0;
    //             feiyan_total.t_title_time = data._updated_at.replace('T', ' ').substring(0, 16);
    //         }
    //     })
    // }
    //     })
    // });

    // else if (prename=='china') {
    // chinafeiyan = [];
    // $(res163).find('.map_block ul li').each(function (i, ele) {
    //     var name = /<strong>(.*)</.exec($(ele).html())[1] || '';
    //     var value = /确诊(.*?)例/.exec($(ele).html())[1] || '';
    //     var value1 = (t = /死亡(.*)例/.exec($(ele).html())) ? t[1] : '';
    //     chinafeiyan.push({
    //         name: name,
    //         value: value,
    //         value1: value1,
    //         overseas: $(ele).hasClass('overseas') ? 1 : 0
    //     })
    // })
    // }

    //显示总数
    for (var i in feiyan_total) {
        if (i.indexOf('t_') == 0) {
            if (i == 't_title_time')
                $('.' + i).text(feiyan_total[i])
            else {
                let demo = new CountUp(i, feiyan_total[i], {
                    duration: 5
                });
                demo.start();
            }
        }
    }
    chinafeiyan_max = chinafeiyan.reduce(function (max, num) {
        if (num.value * 1 > max.value * 1)
            return num
        else
            return max
    }).value;
    //生成分地区标签
    chinafeiyan.forEach(function (element, inx) {
        if (element.value == 0 || element.overseas && element.overseas == 1) return
        var node = document.createElement("div");
        node.className = "provcard animated fadeInLeft";
        const delay = 100;
        $(node).css('animation-delay', inx * delay + "ms")
        $(node).css('-webkit-animation-delay', inx * delay + "ms")
        var label = document.createElement("span");
        label.innerText = element.name;
        label.className = "provlbl"
        var num = document.createElement("span");
        num.className = "provnum"
        num.innerText = element.value;
        $(num).css("background-color", piecesColor(element.value * 1));
        $(num).css("color", piecesTextColor(element.value * 1))
        // num.style = "background-color:rgb("
        //     +linerData(chinafeiyan_min,chinafeiyan_max,255,230,element.value) + ","
        //     +linerData(chinafeiyan_min,chinafeiyan_max,255,50,element.value) + ","
        //     +linerData(chinafeiyan_min,chinafeiyan_max,255,50,element.value) + ","
        //     +")"
        node.appendChild(num);
        node.appendChild(label);

        $('.provincepanel').append(node)
    });
    var chartopt = {
        // geo:{
        //     map: 'china'
        // },
        visualMap: {
            type: 'piecewise',
            text: ['确诊病例', ''],
            showLabel: true,
            // min: 0,
            // max: chinafeiyan_max,
            pieces: feiyan_total.pieces,
            realtime: false,
            calculable: true,
            // inRange: {
            //     color: ['white', 'pink', 'Crimson']
            // }
        },
        series: [{
            type: 'map',
            name: 'feiyan',
            map: prename,
            label: {
                show: true
            },
            data: chinafeiyan
        }]
    };
    myChart.setOption(chartopt);
    // if (bfirst) myChart.setOption(chartopt);
    // else {
    //     myChart.setOption({
    //         series: [{
    //             name: 'feiyan',
    //             map: prename,
    //             data: chinafeiyan
    //         }
    //         ]
    //     });
    // }

    //线性插值
    function linerData(omin, omax, tmin, tmax, input) {
        return tmin + (input - omin) * (tmax - tmin) / (omax - omin);
    }

    //分段映射
    function piecesColor(input) {
        var t = feiyan_total.pieces.find(function (ele) {
            if (ele.min <= input && ele.max >= input) {
                return true;
            }
        });
        if (t) return t.color;
        else return '';
    }
    //分段映射
    function piecesTextColor(input) {
        var t = feiyan_total.pieces.find(function (ele, i) {
            if (ele.min <= input && ele.max >= input && (i == 0 || i == 1)) {
                return true;
            }
        });
        if (t) return "white";
        else return "black";
    }
}


function calcProvData2(sheng, res) {
    chinafeiyan = [];
    res.forEach(function (data) {
        if (data.provinceShortName == sheng) {
            console.log(data)
            data.cities.forEach(function (city) {
                chinafeiyan.push({
                    name: city.cityName,
                    value: city.confirmedCount || 0,
                    value1: city.curedCount || 0,
                    value2: city.deadCount || 0,
                    value3: city.suspectedCount || 0,
                    // createtime: data._created_at,
                    // updatetime: data._updated_at,
                });
            })

        }
    });
    chinafeiyan.sort(function (a, b) {
        if (a.value * 1 < b.value * 1) {
            return 1;
        }
        else if (a.value * 1 > b.value * 1) {
            return -1;
        }
        else {
            return 0;
        }
    });
}

function calcProvData(sheng, res, feiyan_total) {
    chinafeiyan = [];
    res.data.forEach(function (data) {
        if (data.one_level_area == sheng) {
            chinafeiyan.push({
                name: data.two_level_area,
                value: data.sure_cnt || 0,
                value1: data.die_cnt || 0,
                value2: data.cure_cnt || 0,
                value3: data.like_cnt || 0,
                createtime: data._created_at,
                updatetime: data._updated_at,
            });
            feiyan_total.t_num_quezhen += (data.sure_cnt * 1 || 0);
            feiyan_total.t_num_zhiyu += data.cure_cnt * 1 || 0;
            feiyan_total.t_num_siwang += data.die_cnt * 1 || 0;
            feiyan_total.t_num_yisi += data.like_cnt * 1 || 0;
        }
        feiyan_total.t_title_time = res.data.reduce(function (max, num) {
            if (num._updated_at > max._updated_at)
                return num;
            else
                return max;
        })._updated_at.replace('T', ' ').substring(0, 16);
    });
    chinafeiyan.sort(function (a, b) {
        if (a.value * 1 < b.value * 1) {
            return 1;
        }
        else if (a.value * 1 > b.value * 1) {
            return -1;
        }
        else {
            return 0;
        }
    });
}

function calcProvTotal(sheng, res, feiyan_total) {
    res.forEach(function (data) {
        if (data.provinceShortName == sheng) {
            feiyan_total.t_num_quezhen = data.confirmedCount;
            feiyan_total.t_num_zhiyu = data.curedCount;
            feiyan_total.t_num_siwang = data.deadCount;
            feiyan_total.t_num_yisi = data.suspectedCount;
            feiyan_total.t_title_time = new Date(data.modifyTime).toLocaleString();
        }

    });
}

function calcChinaData2(res) {
    chinafeiyan = [];
    res.forEach(function (data) {
        var provdata = {
            name: data.provinceShortName,
            value: data.confirmedCount,
            value1: data.curedCount,
            value2: data.deadCount,
            value3: data.suspectedCount,
            createtime: '',
            updatetime: ''
        };
        chinafeiyan.push(provdata);
    });
    chinafeiyan.sort(function (a, b) {
        if (a.value * 1 < b.value * 1) {
            return 1;
        }
        else if (a.value * 1 > b.value * 1) {
            return -1;
        }
        else {
            return 0;
        }
    });
    console.log(chinafeiyan)
}
function calcChinaData(provs, res, feiyan_total) {
    chinafeiyan = [];
    provs.forEach(function (prov) {
        var provdata = {
            name: prov,
            value: 0,
            value1: 0,
            value2: 0,
            value3: 0,
            createtime: '',
            updatetime: ''
        };
        chinafeiyan.push(provdata);
        res.data.forEach(function (data) {
            if (data.one_level_area == prov && data.one_level_area != '待确') {
                feiyan_total.t_num_quezhen += (data.sure_cnt * 1 || 0);
                feiyan_total.t_num_zhiyu += data.cure_cnt * 1 || 0;
                feiyan_total.t_num_siwang += data.die_cnt * 1 || 0;
                feiyan_total.t_num_yisi += data.like_cnt * 1 || 0;
                provdata.value += (data.sure_cnt * 1 || 0);
                provdata.value1 += data.cure_cnt * 1 || 0;
                provdata.value2 += data.die_cnt * 1 || 0;
                provdata.value3 += data.like_cnt * 1 || 0;
            }
        });
    });
    // console.log(feiyan_total);
    chinafeiyan.sort(function (a, b) {
        if (a.value * 1 < b.value * 1) {
            return 1;
        }
        else if (a.value * 1 > b.value * 1) {
            return -1;
        }
        else {
            return 0;
        }
    });
    //计算最新时间
    feiyan_total.t_title_time = res.data.reduce(function (max, num) {
        if (num._updated_at > max._updated_at)
            return num;
        else
            return max;
    })._updated_at.replace('T', ' ').substring(0, 16);
}

function provList(res) {
    var provs = new Set();
    res.data.forEach(function (data) {
        if (data.country == '中国') {
            var name = data.one_level_area.substring(0, 2);
            if (name == '黑龙')
                name += '江';
            if (name == '内蒙')
                name += '古';
            data.one_level_area = name;
            provs.add(name);
        }
    });
    return provs;
}

//获取url参数
function getURLPars(variable, dft) {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair[0] == variable) { return pair[1]; }
    }
    return dft;
}
